<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
  return view('welcome');
});*/

Route::view('/', "loguin")
  ->name("loguin");

Route::post('/validate', "UsuariosController@validateUser")
  ->name("validateUser");

Route::get('/list', "UsuariosController@index")
  ->name("inicio")
  ->middleware('auth');

Route::post('/add', "UsuariosController@addUser")
	->name('addUser')
	->middleware('auth');

Route::view('/crear', "crear")
  ->name("crearUser")
  ->middleware('auth');

Route::get("/edit/{id}","UsuariosController@updateUsers")
	->name('updateUser');

Route::post("/editUser","UsuariosController@update")
	->name('updateUsers')
	->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logOutUsers','UsuariosController@logOut')
	->name('logoutUser')
	->middleware('auth');
