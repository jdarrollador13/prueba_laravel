<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Usuarios;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemCreateRequest;
use App\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use DB;

class UsuariosController extends Controller
{

	// validar si ya inicio session
	public function showLogin()
	{
		if (Auth::check())
		{
		  // Si tenemos sesión activa mostrará la página de inicio
		   return redirect()
		    ->route('loguin')
	      ->with('mensaje', 'error al iniciar');
		}
		// Si no hay sesión activa mostramos el formulario
		return redirect()
	    ->route('loguin')
      ->with('mensaje', 'error al iniciar');
	}

	//validar usuario
	public function validateUser(Request $request)
  {
      // Obtenemos los datos del formulario
      $data = [
          'nombre_usuario' => $request->nombre,
          'password' =>$request->password
      ];

      if (Auth::attempt($data)) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
      {
        // Si nuestros datos son correctos mostramos la página de inicio
      	return redirect()
	      ->route('inicio')
      	  ->with('mensaje', 'Canción agregada');
      }
      // Si los datos no son los correctos volvemos al login y mostramos un error
      return redirect()
	    ->route('loguin')
      	->with('mensaje', 'error al iniciar');
  }


	// listar los usuarios
	public function index()
	{
		$user = Auth::user();
		//$user->role;
    $users = Usuarios::all();
      return view("inicio")
        ->with("usuarios", $users)
        ->with('role', $user);
	}

	//cerrar session
	public function logOut()
    {
        // Cerramos la sesión
        Auth::logout();
        // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
        return redirect()
	    ->route('loguin')
      	->with('mensaje', 'error al iniciar');
    }


  //agregar usuario
	public function addUser(Request  $request)
	{
	    // Instancio al modelo Jugos que hace llamado a la tabla 'jugos' de la Base de Datos
	    $users = new Usuarios; 
	 
	    // Recibo todos los datos del formulario de la vista 'crear.blade.php'
	    $users->nombre = $request->nombre;
	    $users->nombre_usuario = $request->nombre_usuario;
	    $users->password = bcrypt($request->password);
	    $users->email = $request->email;
	    $users->ciudad = $request->ciudad;
	    $users->pasatiempo = $request->pasatiempo;
	    $users->role = $request->role;

	    $users->save();

	   return redirect()
	    ->route('inicio')
      ->with('mensaje', 'Canción agregada');
	}

	//actualizar un registro
	public function updateUsers(Request $request)
	{
		 $user = Auth::user();
	   $idUser = $request->route("id");
	   $users = Usuarios::findOrFail($idUser);
	   return view("Actualizar")
          ->with("usuarios", $users)
          ->with("role", $user);
	}

	//actualizar un registro
	public function update(Request $request)
	{     
    $idUserActualiza = $request->id_usuario;
    # Obtener modelo fresco de la base de datos
    # Buscar o fallar
    $users = Usuarios::findOrFail($idUserActualiza);
    
   	$users->nombre = $request->nombre;
    $users->nombre_usuario = $request->nombre_usuario;
    $users->email = $request->email;
    $users->ciudad = $request->ciudad;
    $users->pasatiempo = $request->pasatiempo;
    $users->role = $request->role;
    
    $users->save();
 
	  return redirect()
      ->route('inicio')
      ->with('mensaje', 'Canción actualizada');
	}
}
