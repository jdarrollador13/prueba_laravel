<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuarios extends Model
{
  // Instancio la tabla 'jugos' 
  protected $table = 'usuarios';
  // Declaro los campos que usaré de la tabla 'jugos' 
  protected $fillable = ['nombre', 'nombre_usuario', 'password', 'email','ciudad','pasatiempo','role'];

}
