<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{ asset('css/style.css') }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
	</style>
</head>
<body>
@include('menu')
<div class="row" style="width: 100% !important;">
	<div class="col-md-12">
		<section class="panel"> 
			<div class="panel-body">
			<div class="card">
		  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Listar Usuarios</h3>
		  <div class="card-body">
		    <div id="table" class="table-editable">
		      <table class="table table-bordered table-responsive-md table-striped text-center">
			        <thead>
			          <tr>
			            <th class="text-center">Nombre</th>
			            <th class="text-center">Nombre Usuario</th>
			            <th class="text-center">email</th>
			            <th class="text-center">Ciudad</th>
			            <th class="text-center">Acciones</th>
			          </tr>
			        </thead>
			        <tbody>
			        	@foreach ($usuarios as $usuarios)
			          <tr>
			            <td class="pt-3-half" contenteditable="true">{{ $usuarios->nombre }}</td>
			            <td class="pt-3-half" contenteditable="true">{{ $usuarios->nombre_usuario }}</td>
			            <td class="pt-3-half" contenteditable="true">{{ $usuarios->email }}</td>
			            <td class="pt-3-half" contenteditable="true">{{ $usuarios->ciudad }}</td>
			            <td class="pt-3-half">
			              <span class="table-remove"><a href="{{ route('updateUser',['id'=> $usuarios->id]) }}" class="text-success">Editar</a></span>
			            </td>
			          </tr>
			          
			          @endforeach
			        </tbody>
		      </table>
		    </div>
		  </div>
</div>
<!-- Editable table -->
			</div>
		</section>
	</div>
</div>
</body>
</html>