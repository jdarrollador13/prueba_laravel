
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	@if($role->role=='Administrador')
  <a class="navbar-brand" href="{{ route('crearUser') }}" style="font-weight: bold; font-size: 16px;">Craer Usuario</a>
  @endif
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
    </ul>
    <form class="form-inline my-2 my-lg-0">
    	<a class="nav-link disabled" href="#" style="font-weight: bold;">{{ $role->nombre_usuario }}</a>
      <a class="navbar-brand" href="{{ route('logoutUser') }}" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
      <a class="navbar-brand" href="#">
	    <img src="{{ asset('img/u2.png') }}" width="50" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
 			</a>
    </form>
  </div>
</nav>
