<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{ asset('css/style.css') }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
	</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ route('inicio') }}" style="font-weight: bold; font-size: 16px;">Listar Usuarios</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a class="navbar-brand" href="{{ route('logoutUser') }}" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
      <a class="navbar-brand" href="#">
	    <img src="{{ asset('img/u2.png') }}" width="50" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
 	  </a>
    </form>
  </div>
</nav>
<div class="row" style="width: 100% !important;">
	<div class="col-md-12">
		<section class="panel panel-form-form"> 
			<div class="panel-body ">
				<h1  class="title-form">Crear Usuario</h1>
				<form  method="post" action="{{ route('addUser') }}">
					@csrf
					<div class="form-group">
						<label for="nombre" class="negrita label-form">Ingresa Nombre:</label> 
						<div>
							<input class="form-user" placeholder="Ingresa Nombre" required="required" name="nombre" type="text" id="nombre"> 
						</div>
					</div>
				 
					<div class="form-group">
						<label for="nombre_usuario" class="negrita label-form">Nombre Usuario:</label> 
						<div>
							<input class="form-user" placeholder="Nombre Usuario" required="required" name="nombre_usuario" type="text" id="nombre_usuario" > 
						</div>
					</div>
				 
					<div class="form-group">
						<label for="password" class="negrita label-form">Ingrese contraseña:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese contraseña" required="required" name="password" type="password" id="password" > 
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="negrita label-form">Ingrese Email:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese Email" required="required" name="email" type="text" id="email" > 
						</div>
					</div>

					<div class="form-group">
						<label for="ciudad" class="negrita label-form">Ingrese ciudad:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese ciudad" required="required" name="ciudad" type="text" id="ciudad" > 
						</div>
					</div>
					<div class="form-group">
						<label for="role" class="negrita label-form">Seleccione Role:</label> 
						<div>
							<select class="form-user" id="inlineFormCustomSelectPref"  required="required" name="role" id="role">
							  <option value="Administrador">Administrador</option>
							  <option value="Usuario">Usiario</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="pasatiempo" class="negrita label-form">Ingrese pasatiempo:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese pasatiempo" required="required" name="pasatiempo" type="text" id="pasatiempo" > 
						</div>
					</div>
					<input type="submit" class="btn btn-info btn-form-register" value="Guardar">
				</form>	
					<br>
					<br>
			</div>
		</section>
	</div>
</div>
</body>
</html>