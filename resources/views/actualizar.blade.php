<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{ asset('css/style.css') }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
	</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ route('inicio') }}" style="font-weight: bold; font-size: 16px;">Listar Usuarios</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a class="navbar-brand" href="{{ route('logoutUser') }}" tabindex="-1" style="font-weight: bold; font-size: 16px;">Cerrar Sessión</a>
      <a class="navbar-brand" href="#">
	    <img src="{{ asset('img/u2.png') }}" width="50" height="50" class="d-inline-block align-top" alt="" style="border-radius: 10px;">
 	  </a>
    </form>
  </div>
</nav>
<div class="row" style="width: 100% !important;">
	<div class="col-md-12">
		<section class="panel"> 
			<div class="panel-body panel-form-form">
				<h1 class="title-form">Editar Usuario</h1>
				<form method="POST" action="{{ route('updateUsers') }}">
					@csrf
					<input value="{{ $usuarios->id }}" type="hidden" name="id_usuario">
					@if($role->role=='Administrador')

					<div class="form-group">
						<label for="nombre" class="negrita label-form">Nombre:</label> 
						<div>
							<input class="form-user" placeholder="Ingresa Nombre" required="required" name="nombre" type="text" id="nombre" value="{{ $usuarios->nombre }}"> 
						</div>
					</div>
				 
					<div class="form-group">
						<label for="nombre_usuario" class="negrita label-form">Nombre Usuario:</label> 
						<div>
							<input class="form-user" placeholder="Nombre Usuario" required="required" name="nombre_usuario" type="text" id="nombre_usuario" value="{{ $usuarios->nombre_usuario }}" > 
						</div>
					</div>
				 
					<div class="form-group">
						<label for="email" class="negrita label-form">Ingrese Email:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese Email" required="required" name="email" type="email" id="email" value="{{ $usuarios->email }}"	> 
						</div>
					</div>

					<div class="form-group">
						<label for="ciudad" class="negrita label-form">Ingrese ciudad:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese ciudad" required="required" name="ciudad" type="text" id="ciudad" value="{{ $usuarios->ciudad }}"> 
						</div>
					</div>

					<div class="form-group">
						<label for="role" class="negrita label-form">Seleccione Role:</label> 
						<div>
							<select class="form-user" id="inlineFormCustomSelectPref"  required="required" name="role" id="role">
							@if($usuarios->role=='Administrador'){
							  <option value="Administrador">Administrador</option>
							  <option value="Usuario">Usuario</option>
							}
							@else
								<option value="Usuario">Usuario</option>
								<option value="Administrador">Administrador</option>
							@endif
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="pasatiempo" class="negrita label-form">Ingrese pasatiempo:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese pasatiempo" required="required" name="pasatiempo" type="text" id="pasatiempo"  value="{{ $usuarios->pasatiempo }}"> 
						</div>
					</div>

					@else

					<div class="form-group" hidden>
						<label for="nombre" class="negrita label-form">Nombre:</label> 
						<div>
							<input class="form-user" placeholder="Ingresa Nombre" required="required" name="nombre" type="text" id="nombre" value="{{ $usuarios->nombre }}"> 
						</div>
					</div>
				 
					<div class="form-group" hidden>
						<label for="nombre_usuario" class="negrita label-form">Nombre Usuario:</label> 
						<div>
							<input class="form-user" placeholder="Nombre Usuario" required="required" name="nombre_usuario" type="text" id="nombre_usuario" value="{{ $usuarios->nombre_usuario }}" > 
						</div>
					</div>
				 
					<div class="form-group" hidden>
						<label for="email" class="negrita label-form">Ingrese Email:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese Email" required="required" name="email" type="email" id="email" value="{{ $usuarios->email }}"	> 
						</div>
					</div>

					<div class="form-group" hidden>
						<label for="ciudad" class="negrita label-form">Ingrese ciudad:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese ciudad" required="required" name="ciudad" type="text" id="ciudad" value="{{ $usuarios->ciudad }}"> 
						</div>
					</div>

					<div class="form-group" hidden>
						<label for="role" class="negrita label-form">Seleccione Role:</label> 
						<div>
							<select class="form-user" id="inlineFormCustomSelectPref"  required="required" name="role" id="role">
							@if($usuarios->role=='Administrador'){
							  <option value="Administrador">Administrador</option>
							  <option value="Usuario">Usuario</option>
							}
							@else
								<option value="Usuario">Usuario</option>
								<option value="Administrador">Administrador</option>
							@endif
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-top: 28px;">
						<label for="pasatiempo" class="negrita label-form" style="margin-bottom: 13px;">Ingrese pasatiempo:</label> 
						<div>
							<input class="form-user" placeholder="Ingrese pasatiempo" required="required" name="pasatiempo" type="text" id="pasatiempo"  value="{{ $usuarios->pasatiempo }}" style="margin-bottom: 15px;"> 
						</div>
					</div>

					@endif
					<input type="submit" class="btn btn-info btn-form-register" value="Guardar">
				</form>
					<br>
					<br>
			</div>
		</section>
	</div>
</div>
</body>
</html>