<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    </style>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <section class="panel"> 
            <div class="panel-body panel-form-form" style=" margin-top: 160px; width: 45% !important; margin-left: 390px !important;">
                <h1 class="title-form" style="margin-left: 176px !important;">Iniciar Session</h1>
                <form method="POST" action="{{ route('validateUser') }}">
                    @csrf
                    <div class="form-group">
                        <label for="nombre" class="negrita label-form"  style="margin-left: 103px !important; ">Nombre:</label> 
                        <div>
                            <input class="form-user" placeholder="Ingresa Nombre" required="required" name="nombre" type="text" id="nombre"  style="margin-left: 103px !important; "> 
                        </div>
                    </div>
                 
                    <div class="form-group">
                        <label for="password" class="negrita label-form"  style="margin-left: 103px !important; ">Ingrese contraseña:</label> 
                        <div>
                            <input class="form-user" placeholder="Ingrese contraseña" required="required" name="password" type="password" id="password"  style="margin-left: 103px !important; "> 
                        </div>
                    </div>
                    <input type="submit" class="btn btn-info btn-form-register" value="Ingresar" style="margin-left: 195px;
                     margin-top: 18px;">
                </form>
                    <br>
                    <br>
            </div>
        </section>
    </div>
</div>
</body>
</html>
