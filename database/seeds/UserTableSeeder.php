<?php
use App\Usuarios;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder {

  public function run()
  {
    Usuarios::create([
      'nombre'          => 'admin',
      'nombre_usuario'  => 'Administrator',
      'password'        => 'admin',
      'email'           => 'admin@gmail.com',
      'password'        =>  bcrypt('holamundo'),
      'ciudad'          => 'bogota',
      'pasatiempo'      => 'programar',
      'role'            => 'Administrador',
    ]);
  }
}
